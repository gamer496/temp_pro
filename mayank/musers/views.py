from django.shortcuts import render
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect,HttpResponse
from musers.models import Musers
from django.utils import timezone
from musers.forms import RegistrationForm
from django.contrib.auth import authenticate,login,logout
from django.contrib.auth.views import password_reset,password_reset_confirm
from django.core.urlresolvers import reverse
import datetime
import json
import os
import random

# Create your views here.

def index(request):
    return render(request, "musers/index.html", {})

def registration(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect("/")
    else:
        if request.method == "POST":
            form = RegistrationForm(request.POST)
            if form.is_valid():
                datas = {}
                datas['username']   =form.cleaned_data['username']
                datas['password']   =form.cleaned_data['password']
                datas['first_name'] =form.cleaned_data['first_name']
                datas['last_name']  =form.cleaned_data['last_name']
                datas['email']      =form.cleaned_data['email']
                user = User(username = datas["username"], first_name = datas["first_name"], last_name = datas["last_name"], email = datas["email"])
                user.set_password(datas["password"])
                user.save()
                muser = Musers(user = user)
                muser.save()
                return HttpResponseRedirect("/")
            else:
                return render(request,"musers/registration.html",{'form':form})
        else:
            form = RegistrationForm()
            return render(request,"musers/registration.html",{'form':form})

def muser_login(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect("/")
    else:
        if request.method == "POST":
            username = request.POST["username"]
            password = request.POST["password"]
            user = authenticate(username = username, password = password)
            if user:
                login(request, user)
                return HttpResponseRedirect("/")
            else:
                return render(request, "musers/login.html", {})
        else:
            return render(request, "musers/login.html", {})

def muser_logout(request):
    if not request.user.is_authenticated():
        return HttpResponseRedirect("/")
    else:
        logout(request)
        return HttpResponseRedirect(reverse("mainindex"))
