from django.conf.urls import url
from musers import views

urlpatterns=[
    url(r'^$',views.index,name="musershome"),
    url(r'^registration', views.registration, name = "muserregistration"),
    url(r'^login', views.muser_login, name = "muserlogin"),
    url(r'^logout', views.muser_logout, name = "muserlogout")
]
