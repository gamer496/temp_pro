from django import forms
from django.contrib.auth.models import User
from musers.models import Musers
from mayank import settings
from django.template import Template,Context
from django.shortcuts import render
import os
import datetime
import hashlib


class RegistrationForm(forms.ModelForm):
    password        =forms.CharField    (widget = forms.PasswordInput)
    username        =forms.CharField    ()
    email           =forms.EmailField   ()
    first_name      =forms.CharField    (required = False)
    last_name       =forms.CharField    (required = False)

    class Meta:
        model = Musers
        exclude = ('user', 'college', 'contact_no', 'year')
