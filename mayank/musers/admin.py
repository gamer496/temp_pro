from django.contrib import admin
from musers.models import Musers
from django.contrib.auth.models import User

# Register your models here.

class MuserAdmin(admin.ModelAdmin):
    list_display = ('get_username', 'get_email')
    def get_username(self, obj):
        return obj.user.username
    def get_email(self, obj):
        return obj.user.email
    get_username.short_description = 'Username'
    get_email.short_description = 'Email'

admin.site.register(Musers,MuserAdmin)
