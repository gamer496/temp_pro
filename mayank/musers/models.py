from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

class Musers(models.Model):
    user            =models.OneToOneField   (User)
    college         =models.CharField       (max_length = 200, default = "", blank = True)
    year            =models.IntegerField    (default = 0)
    contact_no      =models.IntegerField    (default = 12)

    def __unicode__(self):
        return self.user.username
# Create your models here.
