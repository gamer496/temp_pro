from __future__ import unicode_literals

from django.db import models
from musers.models import Muser
from django.utils import timezone

# Create your models here.

class Questions(models.Model):
    name            =models.CharField   (max_length = 200, unique = True)
    statement       =models.TextField   ()
    testdata        =models.TextField   ()

    def __unicode__(self):
        return self.name
